package pathdata

// Simple parsing of an SVG `path` element's `d` attribute.

// SVG: https://www.w3.org/TR/SVG11/
// SVG Paths: https://www.w3.org/TR/SVG11/paths.html

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// A model for part of an SVG path element's d attribute:
// A command and its parameters.
type PathDataCommand struct {
	// The single letter code for the command ("M", "L", "z", and so on)
	Code string
	// 0 or more parameters, having been parsed into floats.
	Parameter []float64
}

func ToString(commands []PathDataCommand) string {
	s := ""
	for _, command := range commands {
		s += command.Code + " "
		for _, p := range command.Parameter {
			s += fmt.Sprint(p, " ")
		}
	}
	return s
}

// Process pathdata attribute into a sequence of commands
// (a letter and its parameters).
func PathDataCommands(d string) []PathDataCommand {

	commands := []PathDataCommand{}

	// The most recent letter command
	command := ""
	// Numbers parsed since last letter command
	numbers := []float64{}

	l := lex(d)

	for {
		token := l.nextItem()
		if token.typ == itemEOF {
			break
		}
		switch token.val {
		case "h", "H", "l", "L", "m", "M",
                  "q", "Q", "v", "V", "z", "Z":
			if command != "" {
				commands = append(commands, PathDataCommand{command, numbers})
			}
			command = token.val
			numbers = nil
			continue
		default:
			if token.typ == itemCommand {
				log.Fatalf("Unknown command «%s» %v",
					token.val, l)
			}
		}
		if command == "" {
			log.Fatal("in pathdata, command must come before parameters", d)
		}
		f, err := strconv.ParseFloat(token.val, 64)
		if err != nil {
			log.Fatalf("couldn't parse number %v %v",
				token, l)
		}

		numbers = append(numbers, f)
	}
	commands = append(commands, PathDataCommand{command,
		numbers})

	return commands
}

func Absolute(commands []PathDataCommand) []PathDataCommand {
	res := []PathDataCommand{}
	var cx, cy float64
	z := false
	for _, command := range commands {
		if z && command.Code != strings.ToUpper(command.Code) {
			log.Fatal("Relative command after z", commands)
		}
		L := len(command.Parameter)
		switch command.Code {
		case "H":
			if L > 0 {
				cx = command.Parameter[L-1]
			}
			res = append(res, command)
		case "L", "M":
			L &= ^1
			if L > 0 {
				cx = command.Parameter[L-2]
				cy = command.Parameter[L-1]
			}
			res = append(res, command)
		case "V":
			if L > 0 {
				cy = command.Parameter[L-1]
			}
			res = append(res, command)
		case "h":
			ps := []float64{}
			for _, dx := range command.Parameter {
				cx += dx
				ps = append(ps, cx)
			}
			res = append(res, PathDataCommand{"H", ps})
		case "l", "m":
			ps := []float64{}
			for i := 0; i < L; i += 2 {
				cx += command.Parameter[i]
				cy += command.Parameter[i+1]
				ps = append(ps, cx, cy)
			}
			res = append(res, PathDataCommand{strings.ToUpper(command.Code), ps})
		case "v":
			ps := []float64{}
			for _, dy := range command.Parameter {
				cy += dy
				ps = append(ps, cy)
			}
			res = append(res, PathDataCommand{"V", ps})
		case "z", "Z":
			res = append(res, PathDataCommand{"Z", nil})
		}
		z = strings.ToUpper(command.Code) == "Z"
	}
	return res
}

func Regularise(commands []PathDataCommand) []PathDataCommand {
	cs := []PathDataCommand{}
	var cx, cy float64

	last := ""

	for _, command := range commands {
		if strings.ToUpper(command.Code) != command.Code {
			log.Fatal("relative command ", commands)
		}
		if command.Code == "Z" {
			cs = append(cs, command)
			last = command.Code
			continue
		}
		L := len(command.Parameter)
		if L == 0 {
			continue
		}
		switch command.Code {
		case "L", "M":
			cx = command.Parameter[L-2]
			cy = command.Parameter[L-1]
		case "H":
			ps := []float64{}
			for _, x := range command.Parameter {
				ps = append(ps, x, cy)
                                cx = x
			}
			command = PathDataCommand{"L", ps}
		case "V":
			ps := []float64{}
			for _, y := range command.Parameter {
				ps = append(ps, cx, y)
                                cy = y
			}
			command = PathDataCommand{"L", ps}
		default:
			log.Fatal("Forgot to implement command ",
				command)
		}
		if command.Code == "L" && last == "M" {
			cs[len(cs)-1].Parameter =
				append(cs[len(cs)-1].Parameter, command.Parameter...)
		} else {
			cs = append(cs, command)
			last = command.Code
		}
	}
	return cs
}

// Lexer for pathdata string
// (the contents of the d attribute of SVG path elements).
// Written in Pike style: https://talks.golang.org/2011/lex.slide

// After https://talks.golang.org/2011/lex.slide#22
type lexer struct {
	input             string
	start, pos, width int
	state             stateFn
	items             chan item
}

type item struct {
	typ itemType
	val string
}

type itemType int

const (
	itemError itemType = iota // error, see .val for error text
	itemEOF
	itemNumber
	itemCommand
)

const (
	eof rune = -1
)

// https://talks.golang.org/2011/lex.slide#19
type stateFn func(*lexer) stateFn

func lex(input string) *lexer {
	return &lexer{
		input: input,
		state: lexCommand,
		items: make(chan item, 2),
	}
}

func (l *lexer) nextItem() item {
	for {
		select {
		case item := <-l.items:
			return item
		default:
			l.state = l.state(l)
		}
	}
	panic("not reached")
}

// next returns the next rune in the input.
func (l *lexer) next() (rune rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	rune, l.width =
		utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return rune
}

// backup steps back one rune.
// Can be called only once per call of next.
func (l *lexer) backup() {
	l.pos -= l.width
}

func (l *lexer) emit(t itemType) {
	it := item{t, l.input[l.start:l.pos]}
	l.items <- it
	l.start = l.pos
}

func (l *lexer) ignore() {
	l.start = l.pos
}

// error returns an error token and terminates the scan
// by passing back a nil pointer that will be the next
// state, terminating l.run.
func (l *lexer) errorf(format string, args ...interface{}) stateFn {
	l.items <- item{
		itemError,
		fmt.Sprintf(format, args...),
	}
	return nil
}

// The lexer states

func lexCommand(l *lexer) stateFn {
	for {
		r := l.next()
		if r == eof {
			break
		}
		switch {
		case unicode.IsSpace(r):
			l.ignore()
		case 'a' <= r && r <= 'z' || 'A' <= r && r <= 'Z':
			l.emit(itemCommand)
			return lexNumber
		default:
			log.Fatalf("unexpected. «%s» %v\n",
				l.input[l.start:l.pos], l)
		}
	}
	if l.start < l.pos {
		log.Fatalf("bad EOF in lexer; have «%s» left.  %v\n",
			l.input[l.start:l.pos], l)
	}
	l.emit(itemEOF)
	return nil
}

func lexNumber(l *lexer) stateFn {
	for {
		r := l.next()
		if r == eof {
			break
		}
		if r == ',' || unicode.IsSpace(r) {
			l.ignore()
		} else {
			l.backup()
			break
		}
	}

	for {
		r := l.next()
		switch {
		case '0' <= r && r <= '9',
			'.' == r,
			'-' == r,
			'e' == r:

		case eof == r:
			l.backup()
			if l.pos > l.start {
				l.emit(itemNumber)
			}
			l.emit(itemEOF)
			return nil
		case 'a' <= r && r <= 'z',
			'A' <= r && r <= 'Z':
			l.backup()
			if l.pos > l.start {
				l.emit(itemNumber)
			}
			return lexCommand
		default:
			l.backup()
			l.emit(itemNumber)
			return lexNumber
		}
	}
}
